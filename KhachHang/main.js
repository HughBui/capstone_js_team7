import { showCart, itemList } from "./controller.js";
import { getData } from "./service.js";
let gioHang = [];
let productList = [];

const luuLocalStorage = () => {
  let cartJson = JSON.stringify(gioHang);
  localStorage.setItem("cart", cartJson);
};
let dataJson = localStorage.getItem("cart");

if (dataJson) {
  gioHang = JSON.parse(dataJson);
}
const renderDanhSachSP = () => {
  getData
    .dataPhone()
    .then((res) => {
      let productList = res.data;

      itemList(productList);
    })
    .catch((err) => {
      console.log(err);
    });
};
const renderGioHang = () => {
  let totalQuantity = gioHang.reduce((acc, curr) => {
    return acc + curr.quantity;
  }, 0);
  document.getElementById(
    "gioHang"
  ).innerHTML = `<i class="fa fa-shopping-cart"></i> Giỏ hàng (${totalQuantity})`;
};

document.querySelector(".custom-select").addEventListener("change", (event) => {
  let searchValue = event.target.value;
  getData
    .dataPhone()
    .then((res) => {
      const searchList = res.data.filter((item) => {
        if (item.type == searchValue) {
          return item;
        }
      });
      if (searchList.length != 0) {
        itemList(searchList);
      } else {
        itemList(res.data);
      }
    })
    .catch((err) => {
      console.log(err);
    });
});

const handleThemSanPham = (data) => {
  getData
    .getItemInfor(data)
    .then((res) => {
      let index = gioHang.findIndex((item) => {
        return item.id == res.data.id;
      });
      if (index == -1) {
        let newData = { ...res.data, quantity: 1 };

        gioHang.push(newData);
      } else {
        gioHang[index].quantity++;
      }
      renderGioHang();
      luuLocalStorage();
    })
    .catch((err) => {});
};
const handleShowCart = () => {
  showCart(gioHang);
  console.log(gioHang);
};
const handleXoaSanPham = (data) => {
  let index = gioHang.findIndex((item) => {
    return item.id == data;
  });
  if (index != -1) {
    gioHang.splice(index, 1);
  }

  luuLocalStorage();
  showCart(gioHang);
};
const handleTangGiamSL = (id, value) => {
  let index = gioHang.findIndex((item) => {
    return item.id == id;
  });
  if (index != -1 && gioHang[index].quantity == 1 && value == -1) {
    gioHang.splice(index, 1);
  } else if (gioHang[index].quantity >= 1) {
    gioHang[index].quantity += value;
  }
  renderGioHang();
  showCart(gioHang);
  luuLocalStorage();
};

const handleThanhToan = () => {
  gioHang = [];
  renderGioHang();
  luuLocalStorage();
};
renderDanhSachSP();
renderGioHang();
window.handleTangGiamSL = handleTangGiamSL;
window.handleThemSanPham = handleThemSanPham;
window.handleShowCart = handleShowCart;
window.handleXoaSanPham = handleXoaSanPham;
window.handleThanhToan = handleThanhToan;
