const itemList = (list) => {
  let contentHTML = "";
  list.sort((a, b) => {
    return b.id - a.id;
  });
  list.forEach((item) => {
    let contentDiv = /*html*/ `
   <div class="card ml-5">
    <img class="card-img-top mt-3 mb-1" src=${item.img} alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">${item.name}</h5>
      <p class="card-text">$${item.price}</p>
      <p class="card-text">${item.desc}</p>      
      <button class="btn btn-info" onClick="handleThemSanPham
      ('${item.id}')">Add to cart</button>   
    </div>
  </div> `;
    contentHTML += contentDiv;
  });
  document.querySelector(".main").innerHTML = contentHTML;
};

export { itemList };

const showCart = (list) => {
  let totalPrice = list.reduce((acc, currentValue) => {
    return acc + currentValue.price * currentValue.quantity;
  }, 0);
  let contentHTML = "";
  list.forEach((item) => {
    let contentTrTag = /*html*/ `<tr>
    <td>${item.id}</td>
    <td><img src=${item.img} style="width:50px"></td>
    <td>${item.name}</td>
    <td>$${item.price}</td>
    <td>
    <button class="btn btn-danger mr-1" onClick="handleTangGiamSL(${
      item.id
    },-1)">
    -
    </button>
    ${item.quantity}
    <button class="btn btn-success ml-1" onClick="handleTangGiamSL(${
      item.id
    },1)">+</button>
    </td>
    <td>${item.quantity * item.price}</td>
    <td><button class="btn btn-danger" onClick="handleXoaSanPham(${
      item.id
    })"><i class="fa fa-trash-alt"></i></button></td>   
    </tr>
    `;
    contentHTML += contentTrTag;
  });
  let contentFooter = `<tr>
    <td  style="font-size:24px" class="text-dark text-right font-weight-bold" colspan="6">Thành tiền :</td>
    <td style="font-size:24px" class="text-dark text-right font-weight-bold">$${totalPrice}</td>
  </tr>`;
  contentHTML += contentFooter;
  if (list.length == 0) {
    document.querySelector(
      "#cartTbody"
    ).innerHTML = `<td colspan="6" class="text-muted font-weight-bold">Giỏ hàng rỗng</td>`;
  } else {
    document.querySelector("#cartTbody").innerHTML = contentHTML;
  }
};
export { showCart };
