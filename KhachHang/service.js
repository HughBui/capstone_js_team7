const BASE_URL = "https://62b14776196a9e98703270de.mockapi.io";

const getData = {
  dataPhone: () => {
    return axios({
      url: `${BASE_URL}/Products`,
      method: "GET",
    });
  },
  getItemInfor: (id) => {
    return axios({
      url: `${BASE_URL}/Products/${id}`,
      method: "GET",
    });
  },
};

export { getData, BASE_URL };
